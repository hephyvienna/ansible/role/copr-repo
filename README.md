# copr-repo [![Build Status](https://travis-ci.com/hephyvienna/ansible-role-copr-repo.svg?branch=master)](https://travis-ci.com/hephyvienna/ansible-role-copr-repo) [![Ansible Role](https://img.shields.io/ansible/role/40970.svg)](https://galaxy.ansible.com/hephyvienna/copr_repo)

Install [Fedora copr repositories](https://copr.fedorainfracloud.org)

## Role Variables

    copr_repo: []

List of copr repositories. They are describe by a list of hashes
with two attributes (see example playbook).

    - name:  Name of the repository
      owner:  Owner of the repository

## Example Playbook

    - hosts: servers
      roles:
         - role: hephyvienna.copr-repo
           vars:
             copr_repo:
               - name: singularity
                 owner: dliko

## License

MIT

## Author Information

Written by [Dietrich Liko](Dietrich.Liko@oeaw.ac.at) in May 2019

[Institute for High Energy Physics](http://www.hephy.at) of the
[Austrian Academy of Sciences](http://www.oeaw.ac.at)
